<?php

namespace App\Repository;

use App\Entity\LearningProgram;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LearningProgram|null find($id, $lockMode = null, $lockVersion = null)
 * @method LearningProgram|null findOneBy(array $criteria, array $orderBy = null)
 * @method LearningProgram[]    findAll()
 * @method LearningProgram[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LearningProgramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LearningProgram::class);
    }

    // /**
    //  * @return LearningProgram[] Returns an array of LearningProgram objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LearningProgram
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
