<?php

namespace App\DataFixtures;

use App\Entity\Lesson;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;

class LessonFixtures extends Fixture
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager):void
    {
        for ($i = 1; $i < 50; $i++) {
            $lesson = new Lesson();
            $lesson->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fermentum leo et nunc blandit, sed euismod lorem tincidunt. Quisque fringilla viverra magna, iaculis vulputate ligula laoreet eget. Donec nisl sapien, ultricies laoreet molestie et, tempor at turpis. Donec sed enim interdum, dignissim leo ac, efficitur diam. Cras quis urna quis lectus pharetra tempus. Pellentesque ut consequat nibh. Ut auctor odio nec diam pharetra, a mattis turpis finibus. Cras mauris ipsum, viverra in arcu id, finibus tincidunt risus. Ut et eleifend nisl. Donec libero mi, dapibus ut tempus quis, aliquam eu lacus. Aenean id ultrices nisl, sit amet bibendum felis. Pellentesque vel maximus enim. Suspendisse cursus, risus gravida condimentum sagittis, ante sem blandit sem, at laoreet tortor libero semper ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi gravida, ligula eu porta vestibulum, purus risus fermentum sapien, eget facilisis ante sem at massa.');
            $lesson->setName("Lesson name # $i");
            $lesson->setPreviewDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
            $lesson->setType(random_int(0,count(Lesson::LESSON_TYPES)-1));
            $manager->persist($lesson);
        }
        $manager->flush();
    }
}
